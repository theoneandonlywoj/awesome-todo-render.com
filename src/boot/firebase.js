// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from 'firebase/app'

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import 'firebase/analytics'

// Add the Firebase products that you want to use
import 'firebase/auth'

import 'firebase/database'

const firebaseConfig = {
  apiKey: 'AIzaSyCnStyRwuSsrgxZInUICcErYQ4qkfbczAk',
  authDomain: 'awesome-todo-920ec.firebaseapp.com',
  databaseURL: 'https://awesome-todo-920ec.firebaseio.com',
  projectId: 'awesome-todo-920ec',
  storageBucket: 'awesome-todo-920ec.appspot.com',
  messagingSenderId: '323840254903',
  appId: '1:323840254903:web:200d21252ad805240d7e32',
  measurementId: 'G-FRZZY54MF8'
}

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig)
const firebaseAuth = firebaseApp.auth()
const firebaseDb = firebaseApp.database()

export { firebaseAuth, firebaseDb }
